import React from 'react';
import cx from 'classnames';
import style from './header.module.scss';
import { LoginOutlined, GithubOutlined } from '@ant-design/icons';

export default class Header extends React.Component {
    render() {
        return <div className={style.headerContainer}>
            <div className={style.banner}>
                <div className={style.imgContainer}>
                    <a href="https://imgbb.com/">
                        <img src="https://i.ibb.co/k6pXDDS/dd-logo-higher-res.png" alt="dd-logo-higher-res"/>
                    </a>
                </div>
                <div className={style.loginContainer}>
                    <div className={cx(style.button, style.login)}>
                        <LoginOutlined /> Login
                    </div>
                </div>
            </div>
            <div className={style.navigation}>
                <div className={style.groupButton}>
                    <div className={style.buttonItem}>
                        Help
                    </div>
                    <div className={style.buttonItem}>
                        How it works
                    </div>
                    <div className={style.buttonItem}>
                        Tips & tricks
                    </div>
                    <div className={style.buttonItem}>
                        Winners
                    </div>
                    <div className={style.buttonItem}>
                        <GithubOutlined /> <strong style={{marginLeft: "5px"}}>Get started</strong>
                    </div>
                </div>
            </div>
        </div>
    }
}