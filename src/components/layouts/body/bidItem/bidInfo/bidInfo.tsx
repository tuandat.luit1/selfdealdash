import React from 'react';
import styles from './bidInfo.module.scss';
import cx from 'classnames';

export interface bidInfoProps {
    id: string,
    price: number;
    bidder: string;
    timeBids: number;
}
export interface bidInfoState {
    price: number;
    bidder: string;
    isSold: boolean;
    currentPrice: number;
    intervalFunc: NodeJS.Timeout;
    intervalCountDown: NodeJS.Timeout;
    currentTimeBids: number;
    bidding: boolean;
    timeLeft: number;
}

export default class BidInfo extends React.PureComponent<bidInfoProps, bidInfoState> {

    state = {
        price: this.props.price,
        bidder: this.props.bidder,
        isSold: false,
        currentPrice: this.props.price,
        currentTimeBids: 0,
        intervalFunc: setInterval(() => {}),
        intervalCountDown: setInterval(() => {}),
        bidding: false,
        timeLeft: 10000,
    }

    componentDidMount() {
        let bidInterval =  setInterval(() => {
            if(this.state.currentTimeBids < this.props.timeBids) {
                this.setState({
                    currentTimeBids: this.state.currentTimeBids + 1,
                })
            };
            if (this.state.currentTimeBids === this.props.timeBids ) {
                this.setState({
                    isSold: true,
                })
                clearInterval(this.state.intervalFunc);
                clearInterval(this.state.intervalCountDown);
            };
            this.createBidPrice(this.state.currentPrice || this.state.price);
            this.createRandomBidder(8, 'abcdefghijklmnpq1234567890');
            this.setState({
                timeLeft: 10000,
            });
        }, this.createRandomIntervalTime(1, 9) * 1000)
        let countDown = setInterval(() => {
            let cloneTimeLeft = this.state.timeLeft;
            let seconds = Math.floor((this.state.timeLeft % (1000 * 60)) / 1000);
            const html = document.getElementById(this.props.id);
            html && (html.innerHTML = "00:00:" + seconds);
            this.setState({
                timeLeft: cloneTimeLeft - 1000,
            })
        },1000);
        this.setState({
            intervalFunc: bidInterval,
            intervalCountDown: countDown,
        });
    }

    // componentDidUpdate() {
    //     let timeLeft = 10;
    //     let countDown = setInterval(() => {
    //         if(this.state.currentTimeBids === this.props.timeBids) {
    //             clearInterval(countDown);
    //         }
    //         const timer = document.getElementById("timer");
    //         timer && (timer.innerHTML = "00:" + "00:" + timeLeft);
    //         timeLeft = timeLeft - 1;
    //     }, 1000);
    // }

    componentWillUnmount() {
        clearInterval(this.state.intervalFunc)
    }

    formatPrice = (price: number, locale?: string): string => {
        return new Intl.NumberFormat(locale || 'en-US', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2
        }).format(price);
    };

    createRandomBidder = (length: number, chars: string) => {
        if (length > 0) {
            let result = '';
            for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
           return this.setState({
                bidder: result,
            })
        } 
        return this.setState({
            bidder: 'LuTuanDat',
        })
    }

    createRandomBidTimes = (min: number, max: number): number => {
        return Math.round(Math.random() * (max - min) + min);
    }
    
    createBidPrice = (currentPrice: number) => {
        const newPrice = currentPrice + (Math.random() * 10); 
        this.setState({
            currentPrice: newPrice,
        })
    }

    createRandomIntervalTime = (min: number, max: number): number => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    render() {
        const {currentPrice, bidder, isSold } = this.state;
        return (
            <div className={cx(styles.bidInfo)}>
                
                    <div className={styles.currentPrice}>
                        $<span>
                            {this.formatPrice(currentPrice)}
                        </span>
                    </div>
                    <div className={styles.bidder}>
                        {bidder}
                    </div>
                    <div className={styles.timer}>
                        {!isSold && <span id={this.props.id}></span>}
                        {isSold && 
                            <span className={styles.bidClose}>Congratulations</span>
                        }
                    </div>
                </div>
        );
    }
}