import React from 'react';
import styles from './bidItem.module.scss';
import BidInfo from './bidInfo/bidInfo';

export interface bidItemProps {
    id: string,
    itemName: string,
    price: number,
    image: string,
    bidder: string,
    timeBids: number,
}

export interface bidItemState {
    currentBidder: string,
    currentPrice: number,
    timesBid: number,
}

export default class BidItem extends React.Component<bidItemProps, bidItemState> {
    state = {
        currentBidder: this.props.bidder,
        currentPrice: this.props.price,
        timesBid: this.props.timeBids,
    }
    
    createBidPrice = (currentPrice: number): number => {
        const newPrice = currentPrice + (Math.random() * 10); 
        return newPrice;
    }



    render() {
        const {  itemName, image } = this.props;
        const { currentBidder, currentPrice, timesBid } = this.state;
        return (
            <div className={styles.bidItem}>
                <div className={styles.name}>
                    {itemName}
                </div>
                <div className={styles.image}>
                    <img src={image} alt={itemName} />
                </div>
                <BidInfo price={currentPrice} bidder={currentBidder} timeBids={timesBid} id={this.props.id}></BidInfo>
            </div>
        );
    }
}