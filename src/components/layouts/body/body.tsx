import React from 'react';
import styles from './body.module.scss';
import { CaretDownOutlined } from '@ant-design/icons';
import BidItem from './bidItem/bidItem';
import { data } from '../../../utility/data';
import { cloneDeep } from 'lodash';

export default class Body extends React.Component {
    renderMenuBar = () => {
        return (
            <div className={styles.menuBar}>
                <div className={styles.dropdownCategory}>
                    <div className={styles.labelSection}>
                        <span style={{marginRight: "5px"}}>Browser Categories</span> <CaretDownOutlined />
                    </div>
                    {/* <div className={styles.dropdownOptions}>
                        <div className={styles.dropdownItem}>

                        </div>
                        <div className={styles.dropdownItem}>
                            
                        </div>
                    </div> */}
                </div>
                <div className={styles.searchBar}>

                </div>
            </div>
        );
    };

    createRandomBidTimes = (min: number, max: number): number => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }  


    createRandomBidder = (length: number, chars: string): string => {
        if (length > 0) {
            let result = '';
            for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        } 
        return 'LuTuanDat';
    }

    renderBidItem = () => {
        let cloneData = cloneDeep(data);
        cloneData.forEach((e: any) => {
            e['bidder'] = this.createRandomBidder(8, 'abcdefghijklmnpquioyxz0123456789');
            e['timeBids'] = this.createRandomBidTimes(1, 50);
        })
        return cloneData.map((e: any) => { 
            return (<div className={styles.item}>
                    <BidItem itemName={e.itemName} price={e.price} image={e.image} bidder={e.bidder} timeBids={e.timeBids} id={e.id}/>
                </div>)
        })
         
    }

    render() {
        return <div className={styles.bodyContainer}>
            {this.renderMenuBar()}
            <div className={styles.listItemContainer}>
                {this.renderBidItem()}
            </div>
        </div>
    }
}