import React from 'react';
import Header from '../layouts/header/header';
import styles from './main-page.module.scss'; 
import Body from '../layouts/body/body';


export default class MainPage extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <Header></Header>
                <div className={styles.advertisement}>
                    <img src="https://static.dealdash.com/promo/2020/20210630_000000_endofjune_21_live2.png" alt="End of June clearance: Buy bids for only 14 cents each!" />
                </div>
                <Body></Body>
            </div>
        );
    }

    
}